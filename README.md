# WatermarksTest

Angular test project for placing watermarks on image.

## Services

watermarks.service - state service to work with watermarks

imageUpload.service - to upload local images

apiMock.service - implements api.service interface to interact with server, uses https://picsum.photos

## Containers

watermarks - to load base image and to work with watermarks

home - to choose base image size of random image from https://picsum.photos

result - to display results 'sended' to server

## Directives

movable.directive - to move watermark element

resizable.directive - to resize watermark element

