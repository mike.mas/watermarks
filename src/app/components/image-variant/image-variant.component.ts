import { Component, Input, OnInit } from '@angular/core';
import { MockImage } from 'src/app/models/mockImages';

@Component({
  selector: 'wm-image-variant',
  templateUrl: './image-variant.component.html',
  styleUrls: ['./image-variant.component.scss'],
})
export class ImageVariantComponent {
  @Input('image') image!: MockImage;

  constructor() {}
}
