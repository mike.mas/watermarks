import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'wm-watermarks-toolbar',
  templateUrl: './watermarks-toolbar.component.html',
  styleUrls: ['./watermarks-toolbar.component.scss'],
})
export class WatermarksToolbarComponent implements OnInit {
  @Input('showActiveActions') showActiveActions: boolean = false;
  @Input('showSave') showSave: boolean = false;

  @Output('addImage') addImage: EventEmitter<Event> = new EventEmitter();
  @Output('deleteActive') deleteActive = new EventEmitter();
  @Output('save') save = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}
}
