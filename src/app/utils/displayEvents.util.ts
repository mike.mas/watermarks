import { Position } from '../models/position';

export class DisplayEventsUtil {
  static getStartMoveEventName(isTouch: boolean) {
    return isTouch ? 'touchstart' : 'mousedown';
  }

  static getMoveEventName(isTouch: boolean) {
    return isTouch ? 'touchmove' : 'mousemove';
  }

  static getEndMoveEventName(isTouch: boolean) {
    return isTouch ? 'touchend' : 'mouseup';
  }

  static getPosition(event: MouseEvent | TouchEvent): Position {
    if (event instanceof MouseEvent) {
      return { x: event.clientX, y: event.clientY };
    } else {
      return { x: event.touches[0].clientX, y: event.touches[0].clientY };
    }
  }
}
