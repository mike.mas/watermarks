export interface ImageResult {
  dataUrl: string;
  w: number;
  h: number;
}
