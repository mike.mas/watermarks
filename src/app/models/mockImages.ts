export interface MockImage {
  id: string;
  url: string;
  w: number;
  h: number;
}

export const MOCK_IMAGES: Array<MockImage> = [
  {
    id: '09249e97-9257-45c4-b358-55928584e376',
    url: 'https://picsum.photos/800/600',
    w: 800,
    h: 600,
  },
  {
    id: 'cd8b1ea2-fa84-4411-959f-b3fed369f9d1',
    url: 'https://picsum.photos/600/800',
    w: 600,
    h: 800,
  },
  {
    id: '989e9529-490e-41fc-9f0e-58f4681557f2',
    url: 'https://picsum.photos/1024/768',
    w: 1024,
    h: 768,
  },
  {
    id: '2fd3ca7e-8c8e-48cd-b25b-bf61babffba9',
    url: 'https://picsum.photos/768/1024',
    w: 768,
    h: 1024,
  },
  {
    id: '4d86ea1e-b483-4a76-8215-470eaee32b02',
    url: 'https://picsum.photos/1200/800',
    w: 1200,
    h: 800,
  },
  {
    id: '4a755fb0-8c9b-4ea3-af07-fd809bae0af3',
    url: 'https://picsum.photos/800/1200',
    w: 800,
    h: 1200,
  },
];
