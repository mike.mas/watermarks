export type ResizeHandlerType = 'br' | 'bl' | 'tr' | 'tl';
export interface ResizeHandler {
  el: HTMLElement;
  handlerType: ResizeHandlerType;
}
