export interface ElementSize {
  w: number;
  h: number;
}
