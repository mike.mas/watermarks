export interface Watermark {
  id: string;
  width: number;
  height: number;
  x: number;
  y: number;
  isActive: boolean;
  imageData: string;
}
