import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WatermarksService } from 'src/app/services/watermarks.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
})
export class ResultComponent implements OnInit {
  resultData: string = '';

  constructor(
    private watermarksService: WatermarksService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.resultData = this.watermarksService.getData(true);
    this.watermarksService.clear();
  }

  startAgain() {
    this.router.navigate(['/']);
  }
}
