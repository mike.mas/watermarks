import { Component, OnInit } from '@angular/core';
import { MockImage, MOCK_IMAGES } from 'src/app/models/mockImages';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  mockImages: MockImage[] = [];

  constructor() {}

  ngOnInit(): void {
    this.mockImages = MOCK_IMAGES;
  }
}
