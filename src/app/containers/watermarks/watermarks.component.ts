import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { Observable, Subscription, take } from 'rxjs';
import { ElementSize } from 'src/app/models/elementSize';
import { Position } from 'src/app//models/position';
import { Watermark } from 'src/app//models/watermark';
import { ApiService, API_SERVICE } from 'src/app//services/api.service';
import { ImageUploadService } from 'src/app/services/imageUpload.service';
import { WatermarksService } from 'src/app/services/watermarks.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ImageResult } from 'src/app/models/imageResult';

@Component({
  selector: 'app-watermarks',
  templateUrl: './watermarks.component.html',
  styleUrls: ['./watermarks.component.scss'],
})
export class WatermarksComponent implements OnInit {
  loading: boolean = true;

  watermarks: Watermark[] = [];

  activeWatermark: Watermark | null = null;

  baseImg!: ImageResult;

  private watermarksSub!: Subscription;
  private activeWatrmarkSub!: Subscription;

  constructor(
    private watermarksService: WatermarksService,
    private imageUploadService: ImageUploadService,
    @Inject(API_SERVICE) private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.loadBaseImage();

    this.watermarksSub = this.watermarksService.watermarks$.subscribe((arr) => {
      this.watermarks = arr;
    });
    this.activeWatrmarkSub = this.watermarksService.active$.subscribe((obj) => {
      this.activeWatermark = obj;
    });
  }

  ngOnDestroy() {
    this.watermarksSub.unsubscribe();
    this.activeWatrmarkSub.unsubscribe();
  }

  @HostListener('document:keydown.delete', ['$event'])
  onDeleteKey(event: KeyboardEvent) {
    this.deleteActive();
  }

  save() {
    const data = this.watermarksService.getData(true);
    this.apiService
      .saveWatermarks(data)
      .pipe(take(1))
      .subscribe((res) => {
        this.router.navigate(['/result']);
      });
  }

  addImage(event: Event) {
    const target = event.target as HTMLInputElement;
    if (!target.files || target.files.length === 0) return;
    this.imageUploadService
      .addImage(target.files[0])
      .pipe(take(1))
      .subscribe((res) => {
        this.watermarksService.addFromImage(res);
      });
  }

  activate(id: string) {
    this.watermarksService.setActive(id);
  }

  deactivate() {
    this.watermarksService.deactivateAll();
  }

  deleteActive() {
    if (this.activeWatermark)
      this.watermarksService.remove(this.activeWatermark.id);
  }

  move(id: string, event: Position) {
    this.watermarksService.move(id, event);
  }

  validate(data: { pos: Position; size: ElementSize }): boolean {
    const tolerancePx = 5;
    if (data.pos.x < -tolerancePx || data.pos.y < -tolerancePx) return false;

    if (
      data.pos.x + data.size.w > this.baseImg.w + tolerancePx ||
      data.pos.y + data.size.h > this.baseImg.h + tolerancePx
    )
      return false;

    return true;
  }

  resize(id: string, event: { pos: Position; size: ElementSize }) {
    this.watermarksService.resize(id, event.pos, event.size);
  }

  trackById(index: number, item: Watermark) {
    return item.id;
  }

  private loadBaseImage() {
    const id = this.route.snapshot.params['id'];

    this.apiService
      .loadBaseImage(id)
      .pipe(take(1))
      .subscribe((img) => {
        this.baseImg = { dataUrl: img.dataUrl, w: img.w, h: img.h };
        this.loading = false;
      });
  }
}
