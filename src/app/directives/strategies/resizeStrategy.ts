import { Injectable, InjectionToken } from '@angular/core';
import { ElementSize } from '../../models/elementSize';
import { Position } from '../../models/position';
import { ResizeHandlerType } from '../../models/resizeHandler';

export const RESIZE_STRATEGY = new InjectionToken<ResizeStrategy>(
  'ResizeStrategy'
);

export abstract class ResizeStrategy {
  abstract resize(
    startSize: ElementSize,
    startPos: Position,
    change: ElementSize,
    handlerType: ResizeHandlerType
  ): { size: ElementSize; pos: Position };
}
