import { Injectable } from '@angular/core';
import { ElementSize } from '../../models/elementSize';
import { Position } from '../../models/position';
import { ResizeHandlerType } from '../../models/resizeHandler';
import { ResizeStrategy } from './resizeStrategy';
@Injectable()
export class AspectRatioResizeStrategy extends ResizeStrategy {
  private minWidth: number = 20;
  private minHeight: number = 20;

  resize(
    startSize: ElementSize,
    startPos: Position,
    change: ElementSize,
    handlerType: ResizeHandlerType
  ): { size: ElementSize; pos: Position } {
    let wChange = change.w;
    let hChange = change.h;

    let wNew = startSize.w;
    let hNew = startSize.h;
    let xNew = startPos.x;
    let yNew = startPos.y;

    if (handlerType === 'br' || handlerType === 'tr') {
      wNew = startSize.w + wChange;
    } else {
      wNew = startSize.w - wChange;
    }

    if (wNew < this.minWidth) {
      wNew = this.minWidth;
    }

    if (handlerType === 'br' || handlerType === 'bl') {
      hNew = startSize.h + hChange;
    } else {
      hNew = startSize.h - hChange;
    }

    if (hNew < this.minHeight) {
      hNew = this.minHeight;
    }

    const scale = Math.max(wNew / startSize.w, hNew / startSize.h);
    wNew = scale * startSize.w;
    hNew = scale * startSize.h;
    wChange = wNew - startSize.w;
    hChange = hNew - startSize.h;

    if (handlerType === 'bl') {
      xNew -= wChange;
    }

    if (handlerType === 'tr') {
      yNew -= hChange;
    }

    if (handlerType === 'tl') {
      yNew -= hChange;
      xNew -= wChange;
    }

    return { pos: { x: xNew, y: yNew }, size: { w: wNew, h: hNew } };
  }
}
