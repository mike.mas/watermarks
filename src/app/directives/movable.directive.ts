import { DOCUMENT } from '@angular/common';
import {
  Directive,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  NgZone,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { fromEvent, Subscription, tap } from 'rxjs';

import { IS_TOUCH_DEVICE } from '../utils/touch.util';
import { Position } from '../models/position';
import { DisplayEventsUtil } from '../utils/displayEvents.util';
import { ElementSize } from '../models/elementSize';

@Directive({
  selector: '[wmMovable]',
})
export class MovableDirective implements OnInit, OnChanges, OnDestroy {
  @Input('wmMovable') wmMovable: boolean = false;
  @Input('moveEventOnly') moveEventOnly: boolean = true;
  @Input('moveValidate') moveValidate:
    | ((moveData: { pos: Position; size: ElementSize }) => boolean)
    | undefined;
  @Output('move') move: EventEmitter<Position> = new EventEmitter<Position>();

  private element!: HTMLElement;
  private moveSub!: Subscription;
  private isTouch: boolean = false;
  private startPos!: Position;
  private startSize!: ElementSize;
  private mouseClickPos!: Position;

  constructor(
    private elementRef: ElementRef,
    @Inject(DOCUMENT) private document: Document,
    private ngZone: NgZone
  ) {}

  ngOnInit(): void {
    this.element = this.elementRef.nativeElement as HTMLElement;
    this.isTouch = IS_TOUCH_DEVICE;
    if (this.wmMovable) {
      this.enableMove();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    const wmMovableChange = changes['wmMovable'];
    if (wmMovableChange && !wmMovableChange.isFirstChange()) {
      if (wmMovableChange.currentValue) {
        this.enableMove();
      } else {
        this.disableMove();
      }
    }
  }

  ngOnDestroy(): void {
    this.moveSub?.unsubscribe();
  }

  enableMove(): void {
    const moveStartHandler$ = fromEvent<MouseEvent | TouchEvent>(
      this.element,
      DisplayEventsUtil.getStartMoveEventName(this.isTouch)
    ).pipe(
      tap((event: MouseEvent | TouchEvent) => {
        this.startPos = this.getElementPos();
        this.startSize = this.getElementSize();
        this.mouseClickPos = DisplayEventsUtil.getPosition(event);

        const moveHandler = (event: MouseEvent | TouchEvent) => {
          this.calculateAndMove(DisplayEventsUtil.getPosition(event));
        };

        const moveEndHandler = () => {
          this.document.removeEventListener(
            DisplayEventsUtil.getMoveEventName(this.isTouch),
            moveHandler
          );
          this.document.removeEventListener(
            DisplayEventsUtil.getEndMoveEventName(this.isTouch),
            moveEndHandler
          );
        };

        this.document.addEventListener(
          DisplayEventsUtil.getMoveEventName(this.isTouch),
          moveHandler
        );
        this.document.addEventListener(
          DisplayEventsUtil.getEndMoveEventName(this.isTouch),
          moveEndHandler
        );
      })
    );

    this.ngZone.runOutsideAngular(() => {
      this.moveSub = moveStartHandler$.subscribe();
    });
  }

  private disableMove(): void {
    this.moveSub?.unsubscribe();
  }

  private calculateAndMove(pos: Position): void {
    this.moveElement(
      this.startPos.x + (pos.x - this.mouseClickPos.x),
      this.startPos.y + (pos.y - this.mouseClickPos.y)
    );
  }

  private moveElement(x: number, y: number): void {
    if (
      this.moveValidate &&
      !this.moveValidate({ size: this.startSize, pos: { x, y } })
    ) {
      return;
    }

    this.ngZone.run(() => {
      this.move.emit({ x, y });
    });

    if (!this.moveEventOnly) {
      this.element.style.left = x + 'px';
      this.element.style.top = y + 'px';
    }
  }

  private getElementPos(): Position {
    return {
      x: this.element.offsetLeft,
      y: this.element.offsetTop,
    };
  }

  private getElementSize(): ElementSize {
    return {
      w: this.element.offsetWidth,
      h: this.element.offsetHeight,
    };
  }
}
