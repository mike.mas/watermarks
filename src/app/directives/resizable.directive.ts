import { DOCUMENT } from '@angular/common';
import {
  AfterViewInit,
  ContentChild,
  Directive,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  NgZone,
  OnDestroy,
  Output,
  SimpleChanges,
} from '@angular/core';
import { fromEvent, Observable, Subscription, tap } from 'rxjs';
import { IS_TOUCH_DEVICE } from '../utils/touch.util';
import { Position } from '../models/position';
import { ElementSize } from '../models/elementSize';
import { ResizeHandler, ResizeHandlerType } from '../models/resizeHandler';
import { ResizeStrategy, RESIZE_STRATEGY } from './strategies/resizeStrategy';
import { AspectRatioResizeStrategy } from './strategies/aspectRatioResizeStrategy';
import { DisplayEventsUtil } from '../utils/displayEvents.util';

@Directive({
  selector: '[wmResizable]',
  providers: [
    { provide: RESIZE_STRATEGY, useClass: AspectRatioResizeStrategy },
  ],
})
export class ResizableDirective implements AfterViewInit, OnDestroy {
  @ContentChild('brResizeHandler') brHandler!: ElementRef;
  @ContentChild('blResizeHandler') blHandler!: ElementRef;
  @ContentChild('trResizeHandler') trHandler!: ElementRef;
  @ContentChild('tlResizeHandler') tlHandler!: ElementRef;

  @Input('wmResizable') wmResizable: boolean = false;
  @Input('resizeEventOnly') resizeEventOnly: boolean = true;
  @Input('resizeValidate') resizeValidate:
    | ((resizeData: { pos: Position; size: ElementSize }) => boolean)
    | undefined;
  @Output('resize') resize: EventEmitter<{ pos: Position; size: ElementSize }> =
    new EventEmitter<{ pos: Position; size: ElementSize }>();

  private isTouch: boolean = false;
  private resizeHandlers!: ResizeHandler[];
  private element!: HTMLElement;
  private resizeSubs: Subscription[] = [];
  private startPos!: Position;
  private mouseClickPos!: Position;
  private startSize!: ElementSize;

  constructor(
    private elementRef: ElementRef,
    @Inject(DOCUMENT) private document: Document,
    private ngZone: NgZone,
    @Inject(RESIZE_STRATEGY) private resizeStrategy: ResizeStrategy
  ) {}

  ngAfterViewInit(): void {
    this.element = this.elementRef.nativeElement as HTMLElement;
    this.isTouch = IS_TOUCH_DEVICE;
    this.initHandlers();
    if (this.wmResizable) {
      this.enableResize();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    const wmResizableChange = changes['wmResizable'];
    if (wmResizableChange && !wmResizableChange.isFirstChange()) {
      if (wmResizableChange.currentValue) {
        this.enableResize();
      } else {
        this.disableResize();
      }
    }
  }

  ngOnDestroy(): void {
    this.resizeSubs.forEach((sub) => sub.unsubscribe());
  }

  enableResize(): void {
    this.ngZone.runOutsideAngular(() => {
      this.resizeHandlers.forEach((resizeHandler) => {
        this.resizeSubs.push(
          fromEvent<MouseEvent | TouchEvent>(
            resizeHandler.el,
            DisplayEventsUtil.getStartMoveEventName(this.isTouch)
          )
            .pipe(
              tap((event: MouseEvent | TouchEvent) => {
                event.stopPropagation();
                this.handleResize(event, resizeHandler.handlerType);
              })
            )
            .subscribe()
        );
      });
    });
  }

  private handleResize(
    event: MouseEvent | TouchEvent,
    handlerType: ResizeHandlerType
  ) {
    this.startPos = this.getElementPos();
    this.startSize = this.getElementSize();
    this.mouseClickPos = DisplayEventsUtil.getPosition(event);

    const mouseMoveHandler = (event: MouseEvent | TouchEvent) => {
      this.calculateAndResize(
        DisplayEventsUtil.getPosition(event),
        handlerType
      );
    };

    const mouseUpHandler = () => {
      this.document.removeEventListener(
        DisplayEventsUtil.getMoveEventName(this.isTouch),
        mouseMoveHandler
      );
      this.document.removeEventListener(
        DisplayEventsUtil.getEndMoveEventName(this.isTouch),
        mouseUpHandler
      );
    };

    this.document.addEventListener(
      DisplayEventsUtil.getMoveEventName(this.isTouch),
      mouseMoveHandler
    );
    this.document.addEventListener(
      DisplayEventsUtil.getEndMoveEventName(this.isTouch),
      mouseUpHandler
    );
  }

  disableResize(): void {
    this.resizeSubs.forEach((sub) => sub.unsubscribe());
  }

  private calculateAndResize(
    mousePos: Position,
    handlerType: ResizeHandlerType
  ) {
    let wChange = mousePos.x - this.mouseClickPos.x;
    let hChange = mousePos.y - this.mouseClickPos.y;

    const resizeReszult = this.resizeStrategy.resize(
      this.startSize,
      this.startPos,
      { w: wChange, h: hChange },
      handlerType
    );

    this.resizeAndMove(resizeReszult.size, resizeReszult.pos);
  }

  private initHandlers(): void {
    this.resizeHandlers = [];
    if (this.brHandler)
      this.resizeHandlers.push({
        el: this.brHandler.nativeElement,
        handlerType: 'br',
      });

    if (this.blHandler)
      this.resizeHandlers.push({
        el: this.blHandler.nativeElement,
        handlerType: 'bl',
      });

    if (this.trHandler)
      this.resizeHandlers.push({
        el: this.trHandler.nativeElement,
        handlerType: 'tr',
      });

    if (this.tlHandler)
      this.resizeHandlers.push({
        el: this.tlHandler.nativeElement,
        handlerType: 'tl',
      });
  }

  private resizeAndMove(size: ElementSize, pos: Position) {
    if (this.resizeValidate && !this.resizeValidate({ size, pos })) {
      return;
    }

    this.ngZone.run(() => {
      this.resize.emit({ size, pos });
    });

    if (!this.resizeEventOnly) {
      this.resizeElement(size.w, size.h);
      this.moveElement(pos.x, pos.y);
    }
  }

  private getElementPos(): Position {
    return {
      x: this.element.offsetLeft,
      y: this.element.offsetTop,
    };
  }

  private getElementSize(): ElementSize {
    return {
      w: this.element.offsetWidth,
      h: this.element.offsetHeight,
    };
  }

  private resizeElement(width: number, height: number): void {
    this.element.style.width = width + 'px';
    this.element.style.height = height + 'px';
  }

  private moveElement(x: number, y: number): void {
    this.element.style.left = x + 'px';
    this.element.style.top = y + 'px';
  }
}
