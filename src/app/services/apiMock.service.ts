import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, switchMap } from 'rxjs';
import { ImageResult } from '../models/imageResult';
import { MOCK_IMAGES } from '../models/mockImages';
import { ApiService } from './api.service';

@Injectable()
export class ApiMockService implements ApiService {
  constructor(private httpClient: HttpClient) {}

  loadBaseImage(id: string): Observable<ImageResult> {
    const img = MOCK_IMAGES.find((img) => img.id == id);
    if (!img) {
      throw new Error('Mock image not found');
    }

    return this.httpClient
      .get(img.url, { responseType: 'blob' })
      .pipe(switchMap((blob) => this.convertToImageResult(blob, img.w, img.h)));
  }

  saveWatermarks(data: string): Observable<boolean> {
    return of(true);
  }

  private convertToImageResult(
    blob: Blob,
    w: number,
    h: number
  ): Observable<ImageResult> {
    return new Observable<ImageResult>((sub) => {
      const fr = new FileReader();
      fr.onloadend = () => {
        sub.next({ dataUrl: <string>fr.result, w, h });
        sub.complete();
      };

      fr.readAsDataURL(blob);
    });
  }
}
