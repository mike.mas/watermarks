import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ElementSize } from '../models/elementSize';
import { ImageResult } from '../models/imageResult';
import { Position } from '../models/position';
import { Watermark } from '../models/watermark';

@Injectable({
  providedIn: 'root',
})
export class WatermarksService {
  private watermarks: Watermark[] = [];

  private watermarksSubject$: BehaviorSubject<Watermark[]> =
    new BehaviorSubject<Watermark[]>(this.watermarks);

  public readonly watermarks$: Observable<Watermark[]> =
    this.watermarksSubject$.asObservable();

  private activeSubject$: BehaviorSubject<Watermark | null> =
    new BehaviorSubject<Watermark | null>(null);

  public readonly active$: Observable<Watermark | null> =
    this.activeSubject$.asObservable();

  constructor() {}

  addFromImage(imgResult: ImageResult) {
    const id = new Date().valueOf().toString();
    this.add({
      id: id,
      imageData: imgResult.dataUrl,
      width: imgResult.w,
      height: imgResult.h,
      isActive: false,
      x: 0,
      y: 0,
    });

    this.setActive(id);
  }

  add(obj: Watermark) {
    this.watermarks.push(obj);
    this.updateWatermarksSubject();
  }

  remove(id: string) {
    this.watermarks = this.watermarks.filter((w) => w.id !== id);
    this.updateWatermarksSubject();
    this.updateActiveSubject();
  }

  setActive(id: string) {
    const obj = this.watermarks.find((w) => w.id === id);

    if (!obj || obj?.isActive) return;

    this.deactivate();
    obj.isActive = true;

    this.updateWatermarksSubject();
    this.updateActiveSubject();
  }

  deactivateAll() {
    this.deactivate();
    this.updateWatermarksSubject();
    this.updateActiveSubject();
  }

  move(id: string, pos: Position) {
    const obj = this.watermarks.find((w) => w.id === id);
    if (obj) {
      obj.x = pos.x;
      obj.y = pos.y;
    }
    this.updateWatermarksSubject();
  }

  resize(id: string, pos: Position, size: ElementSize) {
    const obj = this.watermarks.find((w) => w.id === id);
    if (obj) {
      obj.x = pos.x;
      obj.y = pos.y;
      obj.width = size.w;
      obj.height = size.h;
    }
    this.updateWatermarksSubject();
  }

  clear() {
    this.watermarks = [];
    this.updateWatermarksSubject();
    this.updateActiveSubject();
  }

  getData(cutImageData: boolean = false): string {
    return JSON.stringify(
      this.watermarks.map((obj) => {
        return {
          id: obj.id,
          x: obj.x,
          y: obj.y,
          w: obj.width,
          h: obj.height,
          data: cutImageData
            ? obj.imageData.substring(0, 100) + '...'
            : obj.imageData,
        };
      })
    );
  }

  private updateWatermarksSubject() {
    this.watermarksSubject$.next(this.watermarks);
  }

  private updateActiveSubject() {
    const obj = this.watermarks.find((w) => w.isActive) ?? null;
    this.activeSubject$.next(obj);
  }

  private deactivate() {
    const obj = this.watermarks.find((w) => w.isActive === true);
    if (obj) {
      obj.isActive = false;
    }
  }
}
