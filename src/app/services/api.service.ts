import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { ImageResult } from '../models/imageResult';

export const API_SERVICE = new InjectionToken<ApiService>('ApiService');

export interface ApiService {
  loadBaseImage(id: string): Observable<ImageResult>;
  saveWatermarks(data: string): Observable<boolean>;
}
