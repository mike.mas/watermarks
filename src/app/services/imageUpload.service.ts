import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ImageResult } from '../models/imageResult';

@Injectable({
  providedIn: 'root',
})
export class ImageUploadService {
  addImage(file: File, maxSideSize = 200): Observable<ImageResult> {
    return new Observable<ImageResult>((sub) => {
      const fr = new FileReader();
      fr.onloadend = () => {
        const frResult: string = <string>fr.result;
        const img = new Image();
        img.onload = () => {
          let w = img.width;
          let h = img.height;

          if (w > h && w > maxSideSize) {
            w = maxSideSize;
            h = Math.floor((img.height * w) / img.width);
          } else if (h > maxSideSize) {
            h = maxSideSize;
            w = Math.floor((img.width * h) / img.height);
          }

          sub.next({ dataUrl: frResult, w, h });
          sub.complete();
        };
        img.src = frResult;
      };

      fr.readAsDataURL(file);
    });
  }
}
