import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WatermarksComponent } from './containers/watermarks/watermarks.component';
import { HomeComponent } from './containers/home/home.component';
import { ResultComponent } from './containers/result/result.component';
import { HttpClientModule } from '@angular/common/http';
import { API_SERVICE } from './services/api.service';
import { ApiMockService } from './services/apiMock.service';
import { MovableDirective } from './directives/movable.directive';
import { ResizableDirective } from './directives/resizable.directive';
import { LoaderComponent } from './components/loader/loader.component';
import { WatermarksToolbarComponent } from './components/watermarks-toolbar/watermarks-toolbar.component';
import { ImageVariantComponent } from './components/image-variant/image-variant.component';

@NgModule({
  declarations: [
    AppComponent,
    WatermarksComponent,
    HomeComponent,
    ResultComponent,
    MovableDirective,
    ResizableDirective,
    LoaderComponent,
    WatermarksToolbarComponent,
    ImageVariantComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],

  providers: [{ provide: API_SERVICE, useClass: ApiMockService }],
  bootstrap: [AppComponent],
})
export class AppModule {}
